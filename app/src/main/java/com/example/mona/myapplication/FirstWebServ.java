package com.example.mona.myapplication;

import android.os.Bundle;
import android.widget.TextView;

import com.example.mona.myapplication.utils.BaseActivity;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import cz.msebera.android.httpclient.Header;

public class FirstWebServ extends BaseActivity {
TextView City,Country,ISP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_web_serv);
        Bind();
        getDataFromServer();
    }


   void Bind(){

        City=findViewById(R.id.City);
        Country=findViewById(R.id.Country);
        ISP=findViewById(R.id.ISP);
    }

    void getDataFromServer (){
        AsyncHttpClient Client=new AsyncHttpClient();
        Client.get("http://ip-api.com/json", new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
//                Toast.makeText(FirstWebServ.this,"error on network",Toast.LENGTH_LONG).show();
                publicMethods.Toast(mContext,"error on network");
//                City.setText("error");

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
//                Toast.makeText(FirstWebServ.this,responseString,Toast.LENGTH_LONG).show();
                publicMethods.Toast(mContext,responseString);
//                Country.setText(responseString);
            }
        });
    }

}
