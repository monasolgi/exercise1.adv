package com.example.mona.myapplication;

import android.app.Application;
import android.content.Context;

import com.orhanobut.hawk.Hawk;

public class initApp extends Application {
    public static Context appContext;
    @Override
    public void onCreate() {
        super.onCreate();
        Hawk.init(this).build();
        appContext=this;

    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
