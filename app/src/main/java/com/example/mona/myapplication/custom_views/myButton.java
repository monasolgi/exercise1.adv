package com.example.mona.myapplication.custom_views;

import android.content.Context;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;

import com.example.mona.myapplication.utils.BaseApplication;

public class myButton extends AppCompatButton {
    public myButton(Context context) {
        super(context);
        this.setTypeface(BaseApplication.typeface);
    }

    public myButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(BaseApplication.typeface);
    }
}
