package com.example.mona.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.example.mona.myapplication.R;

public class subActivity extends AppCompatActivity  {
    EditText Name,Age;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sub_activity);
        Name=findViewById(R.id.Name);
       Age=findViewById(R.id.Age);
       findViewById(R.id.Back).setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
                String NameVal=Name.getText().toString();
                String AgeVal=Age.getText().toString();
               Intent i=new Intent();
                i.putExtra("Name",NameVal);
                i.putExtra("Age",AgeVal);
                if (NameVal.length()>0)
                    setResult(Activity.RESULT_OK, i);
                else
                    setResult(Activity.RESULT_CANCELED);
               finish();



       }
    });
}}
