package com.example.mona.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mona.myapplication.R;
import com.example.mona.myapplication.subActivity;

public class StarterActivity extends AppCompatActivity implements View.OnClickListener {

    TextView result;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_starter);
        result=findViewById(R.id.result);
        findViewById(R.id.FirstActivityBtn).setOnClickListener(this);
        findViewById(R.id.Gpsbtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent gpsintent=new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(gpsintent,3000);

            }
        });

    }


    @Override
    public void onClick(View view) {
        Intent intent = new Intent(this,subActivity.class);
        startActivityForResult(intent,150);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==150)
        {
            if (resultCode==Activity.RESULT_OK ){
                String response=data.getStringExtra("Name");
                response +=" "+data.getStringExtra("Age");
                result.setText(response);

            }
            else
                result.setText("Fields Are Empty dude!!");


        }
        if (requestCode==3000)
            Toast.makeText(this,"Gps",Toast.LENGTH_SHORT).show();
    }
}
