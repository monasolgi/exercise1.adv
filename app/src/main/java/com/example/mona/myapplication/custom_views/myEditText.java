package com.example.mona.myapplication.custom_views;

import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

import com.example.mona.myapplication.utils.BaseApplication;

public class myEditText extends AppCompatEditText {
    public myEditText(Context context) {
        super(context);
        this.setTypeface(BaseApplication.typeface);
    }

    public myEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(BaseApplication.typeface);
    }
}
