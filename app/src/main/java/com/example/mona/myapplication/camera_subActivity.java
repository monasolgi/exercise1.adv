package com.example.mona.myapplication;

import android.Manifest;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.util.Log;

import com.bumptech.glide.Glide;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;


import java.io.File;
import java.util.List;

import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;


public class camera_subActivity extends AppCompatActivity {
    EditText type_name;
    TextView result;
    ImageView onlinePic;
    Button CamBtn;
    ImageView picFromCam;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.camera_sub_activity);
        Log.d("hi", "onCreate: let's go");
        result = findViewById(R.id.result);
        picFromCam=findViewById(R.id.picFromCam);
        String username = getIntent().getStringExtra("username");
        String password = getIntent().getStringExtra("Password");
        result.setText(username + " " + password);
        onlinePic = findViewById(R.id.onlinePic);
        Glide.with(this).load("data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMQEBUQEBIVFRUQFRUVEBAVEhUQEA8VFRUWFhcVFRUYHSggGBolGxUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OFxAPFS0ZHR0tLS0rKystLS0tLSstKy0rLS0tKysrKy0tLSstNy0uLSsrKy4tKy0rKy0rLTg3LS0rK//AABEIAJcBTgMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAAAAQIDBQYEBwj/xABCEAABBAADBAcGBAUCBAcAAAABAAIDEQQSIQUxQVEGEyJhcYGRByMyobHwFEJSwWJygtHhM6IVQ4PxRFNUc5Kywv/EABgBAQEBAQEAAAAAAAAAAAAAAAABAgME/8QAHREBAQEAAgMBAQAAAAAAAAAAAAERAiEDMUESUf/aAAwDAQACEQMRAD8A9VpFJbRa0BCW0IEpFJ1oQNpFJyVA2kUnJUDKRSkRSCOkUpKRSBiVOpLSBiE+ktII0KSkUgYhOpFIGotOpFIG2i0tJMqAtJaXKjKgS0WlyoyoEtGZFJQ1A20Wn9UeSaWoEDkuZJSKRRmRmRSKQGZLnSZUUgXOkMiSkFqBmVLlT6S0iGZUtJ1JQ1BGWoylS5UZUENFCmLEmRBGLSi0/IjIgZqjVSZUmRAzVGqk6tGRBHqjVSZEoCCPVJakIQWoGWktSZUZUEeYpbT8qMqBlotSZUlIGWUtlOpGVAlpLTsqCEDUJSEUga5wAJJAAFkk0ABvJPJec+0jptkjbDgp6zWZ5ozq0aAMY/gTZJI1FBehYrCMlbkkaHNJBLT8LqNixxF8CvJ/bKDs92GkwUj8P1wlbJHG9zITk6vKcg7LT23bhrXcs8rc6WZ9efN2zNnziaS70d1jy7/5Xa9R9n3T575BhcW5z8w91iCLczulI/L/ABncd51seMT7TMrs0lZjvcGtbm73ZQLPfvXpvsPwr5JZpyPdsDWB3BxNlze/TLfiuXk8l4cf1m+muEluPZyktQQ4lheYQe00W1vFze7nW4royrtLs1k20mZPypMqobnRmS5EZEDcyC4p2RGRBJlRlXJ1pThKUR0Uhc/WFL1xQTpcy5+vKOtQdGZGZQdagyoJ8yMy5+sUT9pxNeInSRh53Rl7RIfBpNoO7MkzqAyo6xB0dYgSLnLkloOkyJM6gDkudBNmSWos6TMgmBTg5c+ZGZB02lzLmzIBQdFpFBaLKCe0Byg1SoJy5ICoLSZig6SQkXOXFKHIJJJKFqh2rh2YhpZNGyRp1LHtD26btCrTEPvRcxauHl29RYzbOiWDB0wWH84WEehCx/tcwMuFjgxGHxMsLXPEJw8b3RQt0c4PaxhAadCDz08/VmBede3WInCYd3BuIAPLVjt/ouXj8ecttavLpPjNovkhbO1/vmBrg8GiHtGpHnw4rZdDek7cfAXEBs0RDZ2DdfB7R+l1Hwohea4SIlgPcLHDX7+al6FYv8JtNjTozE3EeRLvg/3ho8yvXOmXsmdJnChJSFVE5eEB4XOhFdOcJOsC57SaoIcydaKQAiAFKCEUjKgdokBCMqMiBbCLCMqAxBDisQI2gm+0asCyBRc53k1rj6Divn3FYOGTFPmdIepfiywyF5Ly3rLuzqfd/mJ3heydKttNweKwbpXBkTuubK86NYXNb1ZJ4CwR/Us4Nh7FfP14kY7M7PkGIJicbzaAHdfDcs0bvZj2FuWFznRtFRPcXOJyudG5mZ3adlczeeDhvXYCqXA7ajxeOyYeRr2Q4dxmymw2SSSPID35Y3nzV3kVikRaXIjIVUIEpCAwo6tAlpAn9Wjq0DbS5kuRL1aBqLTsiTKikJShxQGrIbX6XSxYxuEbhZBUckrnkB75I2XQha0kW4irJ03VZRGxDinOcvAvabLJHioZTLNeIjzmGWS3wUaFNGjAb3DiCt97ONtvkhayR7nCq7RtzeVE6kKasbwyIL00sRkVDw5LmCaIymvaQLQQvNlNpAQuNCgLzv26X+CgAH/iASf+m8Af7vkvRWhZD2vYXNs3Pr7qaNxo6UbbqOPxBJOysns/FFsIaRZyjj3d/l81WbTmLmNnjNOifbXUOy4EOb5XZ9VdsngjibnYRbANO1rv1s8lWY2FghcYz2SSfMmzXPw4ELqj2fZeObiYIsQzdMxrwOWYWW+INjyXTYWJ9k+L6zZ5j/8ATzPYP5XVIPm93otgqqbRAAUJQgl0RmCiKSkE+VGVCEZLlRlQhAZUUhCApACEjyACeSKw3tEgM8kbGi2MBzjgTvHkqvA4CI0MlHgdFrpIOstztBZ14nwVR1TQSAdQdyxWouOj8LI39kAZhRoVfEK/IWXwcgZ23HK1gJe6+y0NFk/JeebT9rkr5iY/dwg9htdst4F55nfXBWVK9rpIvL9he0xr3NEjtHGr5L0jC4trwK47jzV1MdNIpHklpUJlRlSpaQRYiVsbHSyODWRtc97zoGtaLJPcACvJR0t2rtWWQ7Mb1cERoXlaTyzPcD2yNco0FjxO39o+HxU2AkgwkLpDKB1ha5ocGtc0loaSC4muHC+ayHQHZOIdghh5xLhRDKXsc2QRyTW4ucHsItreG+yOVa5o7ejHTadkxwe0WFsoIAzADN3gjQjvFr0ZuosbivKvaKDisVhHYW5HwOeJSxpc1rTk3yDS7buu9V6LsHEl0Ysa0AR3hSVrOliGlUm39pBjgGgZ2gjPpbQaJaD5A+QV+XUCTwF+i8x2zi7cSTqSSe+ytMsn0y2diMbiWz9l/VsyUBleWgl2taE2Ty3rWdA4g0BoBFUC0iiDSrsNj2sc3M78wGUHfZ7la9M9tSYDDtxOGgYTmDHyuPYjLhYBYCC499gCxvtZrXp6G7MG2GlxAsNBALjW6zp6rz/GdOcTJtBmzo8OcKaLp5Jcss2UNLvctHYO4drtA66aa9vs+6W43GR9ZjIGCJ8nVR4iO2U/QAPjc4nKScocOJAritJt2SIU9zQZGAhjg0F7QdS0OO4GgtMvHvaJgsQI48bDjcVIx5LJBNIYww7rDKaGgkEUGrXeyuWcwuEsjntyjKC7M3hqL3LL9OG4rar4mMgfFh4rJ6wtD3vOl0CaAGg8St30DwJgw/Vn8oAHzWbVnppUoKaSktZRMzgqjpy8HASxZczpyyKNvN8j2tbXmbVox335rLdPZpB+HmY0ubh545HsBpzg00a7xdhVXmHSXBTRTSCSYOZBI2I5TQssa45GXuFkWd9Ddemi6V9GPwEbSybr4i7K51BropQA4seATRLdRx014Xb7Y6L4DFYj8U7FFjZSDNCXNbnNAH4u0wkAWPSlZe0XHRP2cI4A52V8ZtrHua1rQQS59UNBVkq72Z0rPYzObxUd6VE8ePvGn/8AK9KK8p9i7rxOIHOFvykA/desEFagbok0TqKMpVDbSWn5SjKUCoSItGTkJtotA+kiS0ZkDlx7RloV6rrBVVjHWSiq3aeILIydwA3LJQY235r+I8+Hh5K66ZlzYBl3yEDwrX9gsdgmPaKOtCzpzP1WOVajR9IsTey8U4aEwPGh/UA0j5rHey7ZkDocRipcKcXNE9jIcNTSO1rmp3Z56ncGmlpy3r4XwtAf1gDQ3VtkkAXy3Bc3Q/opLgcUcTiZmgNBDcPCXuDydAX6AGrNDXU9ykOUcvtV6Hx4djdoYRjYg1zGzQsGVri51BzQNAbIFAa2tT0KxUr2NDmEZALJ039ymx7Tinh83wRm4ofyNP63fqd8hw4k9ezcS1kmTdm3INXG7ROzKGF9+amtblSltFpLRaqHZq15LJ7TAke4u+E/k/KfEcVqt+nNY7Fy5SQeBUpD4JR8IA05cNyuNk4gVWlrLOxOXx4DxS4TFOMgINVosNNzj5ahkcODHfQryDHSjM6+P3uXqWyMaJAWO3iw4fVeT9JYHYeeSA7mGm8MzTq3Xwpa1FXPqMw3g3fKjvv+y3m3dmy4rA4d0TmsxEZilMcttY9zQ/skHcR1hO7XKAe7N9DtmmfEhzgCyD3kgIsOP5R6/wD1K3U2IzE8e7eg4cDsw4HZMjJZQZ3Mmc0Rki5JSXNawH43Zqo1pwpdwJka1z/iIBcDwdWo9V14KLMRYGm60uNgyyDk7h3hSLVe7Dng2137NblBsEXW8VzXRAFPivgB79fQrNDMyLUQcns1U90SE0LXBPGJWkOFtNjxXTiHXpzUMb9zRuGnounpFKQInZPS9dO9XuAIkaWOAIeC1w4EEUR81lsXiWyTGtRuB7weCvdlSZGOeR/ptc4jnlBNfJZa+Mb7G4MmNxbf/KjyHjr1tb/6F6wSvMfYlE5/4zFu/wCa6Ng8Rnkdr/1GL021uMltFlFpMx5Kgsosos8kloDMlspiEZSWUWVGjVBLqkTLKLKBznUCqLFP1NnRW85ppVJiD2j4WlVVdJMQCxnc7Uc9FlZY8rraSLNb7FHlXmrnbmIDntjHDMXfQfK1nZiQddCCa7q0/sudbi96Kwl87pHfDDrdmnG+yPqf6Sr3GPN3V8ua4tisEMETPzStMr/MdkeQLfmuid5++C1J0zb25nYlx3Cr3ErnnflqUmshB7gL1R1hOn36KDbBAicHG9Dp9Apg3uAl0AVgHKl2Q09Uy9+Vt+gtW8btPAqz2fD8yW/uklotaQkkmVpPIErD4rEB2vqthtR9Qv8ABedh3bcpVh08uiTD4oArhxL3at3Xdmjrpz/yuN0laX46/VYabHZUpzdaDvoO37t11z3Lj9pmD/0cW3j7p/Hg5zT6ZvQJdhPzDfwI9QrTFYb8bgpMMTlkYQWk8Cwiz4Ft+qRKo+g8eXDTSajPIGjdRyAk1zFvryXXLisgNCyd5sCvIroxEjY4WwxaNj7DeZreT3k2T3lZzGTC9S0HhZs+QK1Ui92XjyJQSdDu171rca3PHmG8CwvMTiAcgB7RkY1oGmpcOf3ovTofhpZWq3B4jMV2Yh1xuHKiPIi1R4d5ZK5nJxr1VzCc1t4EV6hS0c7H6LojNDxVfhnXouycUb5Jx/qGSSdo9w3+Kr+kGMOHwMszBbqDGVwMjgzN5ZifJTyG3/02PIi0zbLM+BlFXWQgeEjVu+iMZsWTQFa/EYzq8FPIf+XBK70Y6llsA3Lo3/sQf7haDCxDEQS4aQ02ZkkZdWoDxlseF/JYbs6dvsx2d+H2VACO1MDM7h/qG2X35MnotQlbGGgNaKDQA0cgBQHohdGCIJ8U6u9JR5qht+KUJcveikDUJiVGT0JlJaQOpFJuVBYgJW20juWax7qPy8uJWlDe9UO1ou0WlSrGKxDjLM+Rp7IAa3vA4+pK5dsYe2F1gdXZsWRdaiq3966cf7qU/pP1KixU7Cwg7zxN1a562vMRNWMjjadI4sp56BtaqfE/e5cEzx+OkduGQkHnqF1YiTs3zbYHE6A7l0YV0spzGhp92uPakpc0/v8Aunvn1p2l7gNBQF1V77KinF9nXiVFej7Pd2e4UuzCyW2+eqz/AEYxtwsad4blJ72dnXxr5q2wMmgCn0+LTzRSailtEO023C/wXm8Dre+xuJ051/3Xpzm2CDxFLzHa3ucQ7XSyCPC9VnksQYlgJsgGuYvXmuDER1qFaue06gg3uPNV+0Z2sA53u++H91i1pZ9HMUGktPitRCKcZR+gh450ND9Vg9gT9ok7tKWzOMAhJHGm+u9SUqpebjF/mJJvXeb/AHVViBr579/orjFO7QZ3aDh4qrxUlfegC2irkxHVyMfViKRjjXJrwTXovWMO+2gjXcvK3R5iQRpoFuOj2PIibG+7Y0AHgcp0+X0U1SY5mXEuI40VaQPr77lntvY0jEj+UfNWmCxAdQWDCQvyzuaf1GvPUfIhdU0qg21h8r45hueQx/8AMBbT6AjyCgkk36rUQsklytA5H9kbUkLcM8bzIWtA88x+TSoMPpI6Rx/KAOWu/wCgVftDG9ZNlHwQty+Mjj2vSmjxBWt6JO1dA2ni9Ls+jtbV/so2DwJJ07huWX2hiaIawjOSd/Aak68LohaPoybY08wT538lnW25idbQeYCdSaAAAOQ5puYc/mF0c0lFIbTL7/n/AIRfeqH13pQog++foUt/eqBAQltMpBCMnopMLfvVIY/vVBIAhRiNGT7tBJaoek+LjiLM7g0vsAcwNSbV1lPIepVDt7ZEUxDnxsJBJs6ndX7BKrFbXxQc6mkWdwOqotrY5zIwHZSHciTpvsfNbqfYkbtDEOQpxBb/ACkDTyVZJ0Riqwx1/wDvSfUFcsa1xdG8T12EcZHW5pyBxrM1lChoPqu7D49jmZM4c5hyuI1rflv75Ls2T0cjjzDq6a+s9SvzOyihqVn5uh00WIDsLJ7sEnLKe1ZcXHVo11I1I4BaiK7bExj0JGgHqasn0tdmzNrRPyZndo8Dv0/yjaHRLFPdmuJ2o0c5wG/Wxl1UR6LbQ00wxAIIIe5hFf0lQbLo/iBHpqWudmDgNRzB9PmtFgXdrTmavTTgsBszZGPY4kmKjvqQu8aFBbvYcT2t94bdwHdzJSe1W3WO/h9b/ZOzHuTTfL6Io8l0ZODjzavOPatgjGGzNBPWZs2UOIaGtzEurcP3K9EcTy+iz+23Ok7NaaiuBvmK1UsHnvsw2e7aDsQ2R7mthZH1Th+pznjiKIpq0O1+geIEbntkbM4bomAMe4fwl5q/NXexm9RnygDMBuAG4n+5UuOxj3My6a7+Giz+YuvMJcXC1jGxdayfMQ9swEccZa4hwk1JFDu3kDmtr0eYZsGS5wJcTkI3PIO8HlwtT/hCcoyM0BAGUOABOtcuCsMNG9rPDcBQHhSn5XVHtCb3rTuBF94I3hUuP3/za/YV3t8Svje5kWaSux2mtHAVY4cVlDgMZKfeRAUD8MjaO8DS+VKWGunZsrH65tcxAN/Cb4rXbEjfoTl48buv7rzT/hGNYSBh2kcxIARrd6kLd9Guvjj94x2avhGU5TXO9eCzVlV/S/GgYn4tWtAP35qPY3SVglYwu1cQBqBd7qXLjOjUsz3PeXAuNncct8Aq3EeznrHWZXDuIaaTDXs+0yI8I4vF0A6uNg2FkIscJG2OO5VPSyHHw7Pw2GZI6ejRIYXSPaLI6xxJIDeyBXPfoAqzZmExcTAJG5Q4aDOyx3nXTwW6zGsmxhDeyG2Ro49qvBvEqrwbqdksmzZdepN2STx4qodDiGu6wfCzV1vYTQ1OUB17r4KObpLG62xkEu0Dg4Ajga10IWWog2zinOxxY0aGm3WjtDY9b+a2GF2gMDh+tl7Tt7IydXnlpuWN2fj2tkblic9w/Nl6wAmqHYsjQb6Vlj9h42aUy215LQGnMWUK/SR2eGncrIWvVti7VjxcQkjIBr3kRc1z4jycAdN2h4rtrwXgsPQXakcvW4cticNQWyuaQTvIIbxrd9V610Lkx/Ulu0urc9pHVyMsOe3+MUBfeAF0ZaC/D1/wi0uf7tGZUJXgikWi0EWZOspEIyWylBQhAhkSlyEIELlxTHXh6FKhKrkkaP4fQqKShpV+BpCFkhA4A6NpPEbv06+KEII35+QHLj9Uwlw0NfIIQg7IIXbyB6rsgbR3fNCFFdloBQhbQyV2hVVIyydR5hKhKIizvHoU4sad9+g/uhCyFYI+APyC6mhoGgtKhBzlrR+T0IUD2t4s/wByEKhrYwfy/uuqGOtBohClixFNh9bTo8P4/IoQpRPiYs7MvzXINnHurvQhUIdnHhQ8Ak/4aeJb5tQhBJHs5zfzt8m1afBHlNWD5f4SoQdrGqQR95+SELSFyHmlyoQgNUoakQiv/9k=").into(onlinePic);
        // type_name.setText(R.string.name1);

        CamBtn = findViewById(R.id.CamBtn);
        CamBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GetPermission();
                EasyImage.openCamera(camera_subActivity.this, 0);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {
                Glide.with(camera_subActivity.this).load(imageFile).into(picFromCam);
            }
        });
    }

        @Override
        protected void onStart () {
            super.onStart();
            Log.d("start", "onStart: great");

        }

        void GetPermission () {
            Dexter.withActivity(this)
                    .withPermissions(
                            Manifest.permission.CAMERA,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE

                    ).withListener(new MultiplePermissionsListener() {
                @Override
                public void onPermissionsChecked(MultiplePermissionsReport report) {/* ... */}

                @Override
                public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {/* ... */}
            }).check();
        }
    }
