package com.example.mona.myapplication.utils;

import android.app.Application;
import android.graphics.Typeface;

public class BaseApplication extends Application {
    static   BaseApplication baseApp;
    public  static Typeface typeface;

@Override
    public void onCreate(){
    super.onCreate();
    baseApp=this;
     typeface=Typeface.createFromAsset(getAssets(),Constants.appFontName);
}

}
