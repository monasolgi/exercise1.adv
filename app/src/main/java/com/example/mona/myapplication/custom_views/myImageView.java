package com.example.mona.myapplication.custom_views;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

import com.bumptech.glide.Glide;

public class myImageView extends AppCompatImageView {
    Context mContext;
    public myImageView(Context context) {
        super(context);
        mContext=context;
    }

    public myImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext=context;
    }
    public  void load( String url){
        Glide.with(mContext).load(url).into(this);
    }
}
