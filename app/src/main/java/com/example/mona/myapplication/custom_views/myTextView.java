package com.example.mona.myapplication.custom_views;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.example.mona.myapplication.utils.BaseApplication;

public class myTextView extends AppCompatTextView {
    public myTextView(Context context) {
        super(context);
        this.setTypeface(BaseApplication.typeface);
    }

    public myTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(BaseApplication.typeface);
    }
}
