package com.example.mona.myapplication.Adv;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.example.mona.myapplication.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


        LatLng Tehranpars = new LatLng(-18, 65);
        mMap.addMarker(new MarkerOptions().position(Tehranpars).title("Marker in Tehranpars"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(Tehranpars));
    }
}
