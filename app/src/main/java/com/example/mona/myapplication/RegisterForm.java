package com.example.mona.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.mona.myapplication.utils.BaseActivity;

public class RegisterForm extends BaseActivity implements View.OnClickListener {
    EditText username, Password;
    TextView regForm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_form);

        findViewById(R.id.Btn1).setOnClickListener(this);
        username = findViewById(R.id.username);
        Password = findViewById(R.id.Password);
       regForm=findViewById(R.id.regForm);
       regForm.setTypeface(publicMethods.getTypeFace());
        loadData();

    }

    @Override
    public void onClick(View view) {
        //  Toast.makeText(RegisterForm.this,"Registered Successfully",Toast.LENGTH_SHORT).show();
        // Intent gpsIntent=new Intent(Settings.ACTION_BATTERY_SAVER_SETTINGS);
        String userV = username.getText().toString();
        String passV = Password.getText().toString();
        publicMethods.setShared("username", userV);
        publicMethods.setShared("password", passV);

        if (userV.length() == 0) {
            username.setError("نام کاربری را پر نمایید");
            return;
        }
        if (passV.length() == 0) {
            Password.setError("کلمه عبور را پر نمایید");
            return;
        }
                Intent secondIntent=new Intent(RegisterForm.this,camera_subActivity.class);
                secondIntent.putExtra("username",userV);
                secondIntent.putExtra("Password",passV);
                startActivity(secondIntent);
                finish();
    }


    private  void loadData(){
        String storedUserName=publicMethods.getShared("username"," ");
        String storedPass=publicMethods.getShared("password","");
        username.setText(storedUserName);
        Password.setText(storedPass);
    }

}
