package com.example.mona.myapplication;


import android.content.Context;
import android.graphics.Typeface;
import android.widget.Toast;

import com.orhanobut.hawk.Hawk;

public class publicMethods  {
    public  static  void  Toast(Context mContext, String msg){
        Toast.makeText(mContext,msg,Toast.LENGTH_LONG).show();

    }
    public static void setShared(String key,String value){
        Hawk.put(key,value);

    }
    public  static String getShared(String key, String defvalue){
    return     Hawk.get(key,defvalue);


    }

    public static Typeface getTypeFace(){
        return  Typeface.createFromAsset(initApp.appContext.getAssets(),"sans.ttf");
    }
}
